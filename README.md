# CarCar

Team:

* Juan Bartolo - Service
* Devin Lacey - Sales

## Design

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone <<respository.url.here>>

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/

![Img](/images/CarCarDiagram.png)

### Inventory microservice

## models

# Manufacturer
keeps a record of all manufacturers

# Vehiclemodel
records models name and picture and their manufacturer

# Automobile
tracks the year, color, model and vin of the car along with if its been sold

## Endpoints

# Manufacturer

| Action                         | Method | URL
|--------------------------------|--------|------
| List manufacturers             | GET    | http://localhost:8100/api/manufacturers/
| Create a manufacturer          | POST   | http://localhost:8100/api/manufacturers/
| Get a specific manufacturer    | GET    | http://localhost:8100/api/manufacturers/:id/
| Update a manufacturer	         | PUT    | http://localhost:8100/api/manufacturers/:id/
| Delete a manufacturer          | DELETE | http://localhost:8100/api/manufacturers/:id/

JSON body to send data:
{
  "name": "Bill"
}

The return value of creating, viewing, updating a single manufacturer:
{
	"href": "/api/manufacturers/2/",
	"id": 1,
	"name": "Josh 1"
}

Getting a list of manufacturers return value:
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Josh 1"
		},
		{
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Bill"
		}
	]
}

# Vehiclemodel

| Action                         | Method | URL
|--------------------------------|--------|------
| List vehicle models            | GET    | http://localhost:8100/api/models/
| Create a vehicle model         | POST   | http://localhost:8100/api/models/
| Get a specific vehicle model   | GET    | http://localhost:8100/api/models/:id/
| Update a vehicle model	     | PUT    | http://localhost:8100/api/models/:id/
| Delete a vehicle model         | DELETE | http://localhost:8100/api/models/:id/

Create and update a vehicle model (SEND THIS JSON BODY):
{
  "name": "x",
  "picture_url": "image.coolcar.com"
  "manufacturer_id": 1
}


Updating a vehicle model can take the name and/or picture URL:
{
  "name": "x",
  "picture_url": "image.coolcar.com"
}

Return value of creating or updating a vehicle model:
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "x",
  "picture_url": "image.yourpicoolcarctureurl.com",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Josh 1"
  }
}

Getting a List of Vehicle Models Return Value:
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "x",
      "picture_url": "image.coolcar.com",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Josh 1"
      }
    },
    {
      "href": "/api/models/2/",
      "id": 1,
      "name": "Sonata",
      "picture_url": "image.coolercar.com",
      "manufacturer": {
        "href": "/api/manufacturers/2/",
        "id": 2,
        "name": "Bill"
      }
    }
  ]
}


# Automobile
-- The Automobiles model uses the cars vin in place of an id in both the database and in the urls
-- The vin is UNIQUE and can NOT be used by more than one automobile

| Action                      | Method | URL
|-----------------------------|--------|------
| List automobiles            | GET    | http://localhost:8100/api/automobiles/
| Create a automobile         | POST   | http://localhost:8100/api/automobiles/
| Get a specific automobile   | GET    | http://localhost:8100/api/automobiles/:vin/
| Update a automobile	      | PUT    | http://localhost:8100/api/automobiles/:vin/
| Delete a automobile         | DELETE | http://localhost:8100/api/automobiles/:vin/

Create an automobile (SEND THIS JSON BODY):
{
  "color": "black",
  "year": 1998,
  "vin": "1H347DYA64JR8G1NH",
  "model_id": 1
}
Return Value of Creating an Automobile:
{
	"href": "/api/automobiles/1H347DYA64JR8G1NH/",
	"id": 1,
	"color": "black",
    "year": 1998,
	"vin": "1H347DYA64JR8G1NH",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "X",
		"picture_url": "image.coolcar.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Josh 1"
		}
	}
}

To get the details of a specific automobile, you can query by its VIN, return Value:
{
  "href": "/api/automobiles/1H347DYA64JR8G1NH/",
  "id": 1,
  "color": "black",
  "year": 1998,
  "vin": "1H347DYA64JR8G1NH",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "x",
    "picture_url": "image.coolcar.com",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Josh 1"
    }
  }
}

Getting a list of Automobile Return Value:
{
  "autos": [
    {
      "href": "/api/automobiles/1H347DYA64JR8G1NH/",
      "id": 1,
      "color": "black",
      "year": 1998,
      "vin": "1H347DYA64JR8G1NH",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "x",
        "picture_url": "image.coolcar.com",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Josh 1"
        }
      }
    },
    {
      "href": "/api/automobiles/H18HHA6DBNGTAI625/",
      "id": 2,
      "color": "white",
      "year": 2015,
      "vin": "H18HHA6DBNGTAI625",
      "model": {
        "href": "/api/models/1/",
        "id": 2,
        "name": "Sonata",
        "picture_url": "image.coolercar.com",
        "manufacturer": {
          "href": "/api/manufacturers/2/",
          "id": 2,
          "name": "Bill"
        }
      }
    }
  ]
}

## Service microservice

Hello and welcome to the wonderful world of service! The service microservice is an extension of the dealership that looks to provide repairs and maintenece on your vehicle.

Bring in your automobiles for service and our highly skilled technicians will take care of you. If you purchased a vehicle through us here at CarCar we will automatically mark you as a VIP and give you free extra perks.

The service microservice has 3 models as follow, Technician, AutomobileVO, and a Appointment model. These models power the backend of the service microservice.

THe AutomobileVO is a value object that gets data about the automobiles in the invetory using a poller. The poller gets the vin information of the car and if it has been sold.

Below various API endpoints for service along with format needed to send date to each componenet will be showsn.

The service mirosevice has:
1. Technician Staff
2. Service Appointments

### Technicians

Technician Model has three key values stored, "first_name", "last_name", and "employee_id". These values are what our api will return and what we ened to provide to create a new technician.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Technician detail | GET | http://localhost:8080/api/technicians/<int:pk>/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:pk>/

LIST TECHNICIANS: Following this endpoint will give you a list of all technicians that work for CarCar. Since this is a GET request no data needs to be provided.

After puttin in the request you will get this:

{
	"technicians": [
		{
			"first_name": "Juan",
			"last_name": "Bartolo",
			"employee_id": "JB6565",
			"id": 1
		},
	]
}

TECHNICIAN DETAIL: This is another GET request that needs a 'id' to be passed in so where it says <id:pk> replace that with the id you get from listing all technicians. Using the above example the url would be: http://localhost:8080/api/technicians/1/
This would return:

{
    "first_name": "Juan",
    "last_name": "Bartolo",
    "employee_id": "JB6565",
    "id": 1
}

CREATE TECHNICIAN: If we onboarded a new hire to CarCar we would the need to use a POST request to create the technician in our database. Making a POST request is as easy as using the information from a current technician and changing the name and employee_id.
Use the format Below in the JSON body:

{
	"first_name": "Devin",
	"last_name": "Lacey",
	"employee_id": "DL7878"
}

This will then return you a technician detail with all the informaiton you put in and their newly created id like below:
{
	"first_name": "Devin",
	"last_name": "Lacey",
	"employee_id": "DL7878",
    "id" : 2
}

DELETE TECHNICIAN: When the payroll is getting to big for CarCar and we need to promote technicians to customer we would need to use a DELETE request. You will need the id of the technician and pass it in to the delete api url like this: http://localhost:8080/api/technicians/2/

This will return true or false depending if a techncician exists with that id:
{
	"deleted": true
}

As long as you follow eveything correctly you will be able to now see all your technicians, a single technician, or delete a technician. If you get errors make sure that you are passing in the date JSON is expecting since it only want to look for data that can be expected by the Technician Model.

### Service Apointments

The Appointment Model has 8 values in vin, vip, customer, date, time, technician, reason, and status. Most of these values will be used to create new appointments and the rest will all be used as information on the backend to track completion of a vehicle.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List service appointments | GET | http://localhost:8080/api/appointments/
| Service appointment detail | GET | http://localhost:8080/api/appointments/<int:id>/
| Create service appointment | POST | http://localhost:8080/api/appointments/
| Delete service appointment | DELETE | http://localhost:8080/api/appointments/<int:id>/
| Cancel service appointment | PUT | http://localhost:8080/api/appointments/<int:id>/cancel/
| Finish service appointment | PUT | http://localhost:8080/api/appointments/<int:id>/finish/

LIST SERVICE APPOINTMENTS: This will return a list of all current service appointments. Using the GET request will return:

{
	"appointments": [
		{
			"vin": "1C3CC5FB2AN120174",
			"vip": true,
			"customer": "Josh",
			"date": "2024-01-11",
			"time": "01:25:00",
			"technician": {
				"first_name": "Juan",
				"last_name": "Bartolo",
				"employee_id": "JB6565",
				"id": 1
			},
			"reason": "Oil Change",
			"status": "create",
			"id": 1
		}
	]
}

This will return all the information that is tied to evey appoinment in the databse.

SERVICE APPOINTMENT DETAIL: This will return the detail of a single appointment. Using a GET request at the follwoing url: 	http://localhost:8080/api/appointments/7/ will return:

{
    "vin": "1C3CC5FB2AN120174",
    "vip": true,
    "customer": "Josh",
    "date": "2024-01-11",
    "time": "01:25:00",
    "technician": {
        "first_name": "Juan",
        "last_name": "Bartolo",
        "employee_id": "JB6565",
        "id": 1
    },
    "reason": "Oil Change",
    "status": "create",
    "id": 1
}

CREATE A SERVICE APPOINTMENT: This will create a service appointment with the data input. Key things to note are that for the technician JSON input you only need to provide the "id" if the technician and that will automatically add the rest of the technicians information on the back end. VIP and status also don't need to be put in the JSON body since that is information that gets tracked in the back to see if a customer is a VIP and when a appointment is created the status will be "created".

Passing in the following:
{
    "vin": "2C36C5FZ2AN159823",
    "customer": "Josh",
    "date": "2024-01-11",
    "time": "01:25:00",
    "technician": 1,
    "reason": "Oil Change"
}
Will return:
{
	"vin": "2C36C5FZ2AN159823",
	"vip": false,
	"customer": "Bill",
	"date": "2024-01-11",
	"time": "01:25",
	"technician": {
		"first_name": "Juan",
		"last_name": "Bartolo",
		"employee_id": "JB6565",
		"id": 1
	},
	"reason": "Oil Change",
	"status": "created",
	"id": 2
}

DELETE SERVICE APPOINTMENT: Using a DELETE request is as easy as last time since all you need is to pass and "id" into the delete url. An example url is http://localhost:8080/api/appointments/1/ and will return:

{
	"deleted": true
}

CANCEL SERVICE APPOINTMENT: Using a PUT request is what we will use to update an existing service appointment. Using the list appointments we can get the id of the appointment we want to update. an example url is http://localhost:8080/api/appointments/2/cancel/ and will return:

{
	"vin": "2C36C5FZ2AN159823",
	"vip": false,
	"customer": "Bill",
	"date": "2024-01-11",
	"time": "01:25",
	"technician": {
		"first_name": "Juan",
		"last_name": "Bartolo",
		"employee_id": "JB6565",
		"id": 1
	},
	"reason": "Oil Change",
	"status": "canceled",
	"id": 2
}

All the information of the appointment is still saved, but the satus changed to canceled.

FINISH SERVICE APPOINTMENT: The finale PUT request is eqactly like the cancel appointment but this time we are going to finish the appointment. Using the url of 	http://localhost:8080/api/appointments/2/finish/ will return:

{
	"vin": "2C36C5FZ2AN159823",
	"vip": false,
	"customer": "Bill",
	"date": "2024-01-11",
	"time": "01:25",
	"technician": {
		"first_name": "Juan",
		"last_name": "Bartolo",
		"employee_id": "JB6565",
		"id": 1
	},
	"reason": "Oil Change",
	"status": "finished",
	"id": 2
}

As you can see the only thing that changed was the status to finished.

And thats is eveything in the service microservice. Now you can do everything you need to do in the insomnia with the service api.


### Sales microservice

## Models

# AutomobileVO
value object that stores the required data for this microservice by using a poller to get the data from inventory.

# Customer
keeps track of previous and new customers.

# Salesperson
keeps track of current and new salespeople.

# Sales
interacts with the previous three models and uses the polled data from inventory to display the available cars that can be sold as well as keeping a history of the previously made sales.

## Endpoints

# Customers

| Action                    | Method | URL
|---------------------------|--------|------
| List customers            | GET    | http://localhost:8090/api/customers/
| Create a customer         | POST   | http://localhost:8090/api/customers/
| Show a specific customer  | GET    | http://localhost:8090/api/customers/id/

To create a Customer (SEND THIS JSON BODY):
{
	"first_name": "First",
	"last_name": "Last",
	"address": "Address",
	"phone_number": 1234567890
}

Return Value of Creating a Customer:
{
	"first_name": "First",
	"last_name": "Last",
	"address": "Address",
	"phone_number": 1234567890
}

Return value of Listing all Customers:
[
	{
		"first_name": Gullible",
		"last_name": "Sponge",
		"address": "Pineapple Under the Sea",
		"phone_number": 8172769834,
		"id": 1
	},
	{
		"first_name": "Normal",
		"last_name": "Person",
		"address": "Some Street",
		"phone_number": 619555629,
		"id": 2
	},
	{
		"first_name": "Some",
		"last_name": "Dude",
		"address": "some House",
		"phone_number": 8639871237,
		"id": 3
	}
]

# Salesperson

| Action                    | Method | URL
|---------------------------|--------|------
| List salespeople          | GET    | http://localhost:8090/api/salespeople/
| Create a salesperson      | POST   | http://localhost:8090/api/salespeople/
| Delete a salesperson      | DELETE | http://localhost:8090/api/salespeople/id/

To create a salesperson (SEND THIS JSON BODY):
{
    "first_name": "First",
    "last_name": "Last",
    "employee_id": "Flast"
}

Return Value of creating a salesperson:
{
	"first_name": "First",
	"last_name": "Fast",
	"employee_id": "Flast",
	"id": 4
}

List all salespeople Return Value:
[
	{
		"first_name": "Con",
		"last_name": "Artist",
		"employee_id": "Cartist",
		"id": 1
	},
	{
		"first_name": "Good",
		"last_name": "Guy",
		"employee_id": "Gguy",
		"id": 2
	}
]

# Sales

| Action                    | Method | URL
|---------------------------|--------|------
| List sales                | GET    | http://localhost:8090/api/sales/
| Create a sale             | POST   | http://localhost:8090/api/sales/
| Delete a sale             | DELETE | http://localhost:8090/api/sales/id/

List all Sales Return Value:
[
	{
		"price": "salesperson": {
			"first_name": "con",
			"last_name": "artist",
			"employee_id": "Cartist",
			"id": 1
		},
		"customer": {
			"first_name": "gullible",
			"last_name": "dude",
			"address": "pineapple under the sea",
			"phone_number": 1111,
			"id": 1
		},
		"automobile": {
			"vin": "shoe",
			"sold": true
		},
		"id": 1
	},
    {
		"price": 75000,
		"salesperson": {
			"first_name": "Good",
			"last_name": "Guy",
			"employee_id": "Gguy",
			"id": 2
		},
		"customer": {
			"first_name": "Normal",
			"last_name": "Person",
			"address": "Some Street",
			"phone_number": 619555629,
			"id": 2
		},
		"automobile": {
			"vin": "car",
			"sold": true
		},
		"id": 2
	}
]
