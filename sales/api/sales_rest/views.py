from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .models import  Sale, Customer, Salesperson, AutomobileVO
from .encoders import SalespersonEncoder, SaleEncoder, CustomerEncoder

# Create your views here.
@require_http_methods(['GET', 'POST', 'DELETE'])
def api_salespeople(request, pk=None):
    if request.method == 'GET':
        salespeople = Salesperson.objects.all()
        return JsonResponse(salespeople, SalespersonEncoder, False)
    
    elif request.method == 'DELETE':
        count, _ = Salesperson.objects.filter(id=pk).delete()
        return JsonResponse({'response': count > 0})

    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(salesperson, SalespersonEncoder, False)
        except:
            return JsonResponse(
                {"message": "Invalid Json Body"},
                status=400,
            )
    
@require_http_methods(['GET', 'POST', 'DELETE'])
def api_customer(request, pk=None):
    if request.method == 'GET':
        salespeople = Customer.objects.all()
        return JsonResponse(salespeople, CustomerEncoder, False)
    
    elif request.method == 'DELETE':
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({'response': count > 0})

    else:
        try:
            content = json.loads(request.body)
            salesperson = Customer.objects.create(**content)
            return JsonResponse(salesperson, CustomerEncoder, False)
        except:
            return JsonResponse(
                {"message": "Invalid Json Body"},
                status=400,
            )
    

@require_http_methods(['GET', 'POST', 'DELETE'])
def api_sale(request, pk=None):
    if request.method == 'GET':
        salespeople = Sale.objects.all()
        return JsonResponse(salespeople, SaleEncoder, False)
    
    elif request.method == 'DELETE':
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse({'response': count > 0})

    else:
        try:
            content = json.loads(request.body)
            automobile = AutomobileVO.objects.get(vin=content['automobile'])
            content['automobile'] = automobile

            salesperson = Salesperson.objects.get(id=content['salesperson'])
            content['salesperson'] = salesperson

            customer = Customer.objects.get(id=content['customer'])
            content['customer'] = customer

            sale = Sale.objects.create(**content)
            return JsonResponse(sale, SaleEncoder, False)
        
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid vin"},
                status=400,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee id"},
                status=400,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid phone number"},
                status=400,
            )
        except:
            return JsonResponse(
                {"message": "Invalid Json Body"},
                status=400,
            )
        
    

    
