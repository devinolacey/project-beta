from common.json import ModelEncoder

from .models import AutomobileVO, Sale, Salesperson, Customer

# displays all information listed
# add or remove information through the properties of the encoder
# property name must be findable inside the model

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'vin',
        'sold',
    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        'first_name',
        'last_name',
        'employee_id',
        'id'
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        'first_name',
        'last_name',
        'address',
        'phone_number',
        'id'
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        'price',
        'salesperson',
        'customer',
        'automobile',
        'id'
    ]
    encoders = {
        'salesperson': SalespersonEncoder(),
        'customer': CustomerEncoder(),
        'automobile': AutomobileVOEncoder(),
    }