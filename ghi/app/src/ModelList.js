
function ModelList ({models}) {

    return (
        <div>
        <table className="table table-striped table-hover">
            <thead>
            <tr>
                <th>Name</th>
                <th>Manufacturer</th>
                <th>Picture</th>
            </tr>
            </thead>
            <tbody>
            {models.map(model => {
                return (
                <tr key={model.id}>
                    <td>
                        {model.name}
                    </td>
                    <td>
                        {model.manufacturer.name}
                    </td>
                    <td>
                        <img src={model.picture_url} />
                    </td>
                </tr>
                );
            })}
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
    )
}

export default ModelList;