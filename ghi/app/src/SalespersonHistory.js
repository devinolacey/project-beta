import { useState } from "react";

function SalespersonHistory ({  sales, salespeople }) {
    const [salesperson, setSalesperson] = useState('')
    const [history, setHistory] = useState([])

    const handleSalespersonChange = (e) => {
        setSalesperson(e.target.value)
    };

        

    return (
        <div className="container">
            <h1>Salesperson History</h1>
            <div className="mb-3">
                        <select onChange={handleSalespersonChange} required id="salesperson" name= "salesperson" className="form-select">
                            <option value="">Select a salesperson</option>
                            {salespeople.map(salesperson => {
                                return (
                                    <option key={salesperson.id} value={salesperson.id}>
                                        {salesperson.first_name + " " + salesperson.last_name}
                                    </option>
                                );
                        })}
                        </select>
                    </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        if (sale.salesperson.id === Number(salesperson)){
                        return (
                            <tr key={sale.automobile.vin}>
                                <td>{sale.salesperson.first_name + " " + sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{'$' + sale.price}</td>
                            </tr>
                        );
                        }
                    })}
                </tbody>
            </table>

        </div>
    )

}

export default SalespersonHistory;