import React, { useState } from "react";


function ServiceHistory({ appointments }) {
    const [search, setSearch] = useState('');
    const [filter, setFilter] = useState('');

    function vipCheck(vip) {
        if (vip) {
            return "Yes";
        } else {
            return "No";
        }
    }

    const handleSearchChange = (event) => {
        const value = event.target.value;
        setSearch(value);
    }

    function searchResults(filterResults) {
        setFilter(filterResults);
    }



    return (
        <div className="container">
            <h1>Service Appointments</h1>
            <div className="input-group mb-3">
                <input onChange={handleSearchChange} value={search} type="text" className="form-control" placeholder="Search by VIN..." aria-label="Recipient's username" aria-describedby="button-addon2" />
                <button onClick={() => searchResults(search)} className="btn btn-outline-secondary" type="button" id="button-addon2">Search</button>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        if (filter) {
                            if (filter === appointment.vin) {
                                return (
                                    <tr key={appointment.id}>
                                        <td>{appointment.vin}</td>
                                        <td>{vipCheck(appointment.vip)}</td>
                                        <td>{appointment.customer}</td>
                                        <td>{appointment.date}</td>
                                        <td>{appointment.time}</td>
                                        <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{appointment.status}</td>
                                    </tr>
                                );
                            }
                        } else {
                            return (

                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{vipCheck(appointment.vip)}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.time}</td>
                                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.status}</td>
                                </tr>
                            );
                        }
                    })}
                </tbody>
            </table>
        </div>
    );

}



export default ServiceHistory;
