import { useNavigate } from "react-router-dom"
import { useState } from "react";

function SaleForm({getSales, automobiles, salespeople, customers}) {
    const navigate = useNavigate()
    const soldCar = {'sold': true}
    const [formData, setFormData] = useState({
        automobile: '',
        salesperson: '',
        customer: '',
        price: '',
    })

    const handleFormChange = (e) => {
        const inputName = e.target.name
        const value = e.target.value
        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const saleUrl = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(saleUrl, fetchConfig)
        

        if (response.ok) {
            const soldUrl = `http://localhost:8100/api/automobiles/${formData.automobile}/`
            const fetchCarConfig = {
            method: 'put',
            body: JSON.stringify(soldCar),
            headers: {
                'Content-Type': 'application/json',
            }
            }
            const sold = await fetch(soldUrl, fetchCarConfig)
            
            if (sold.ok) {
                const newSale = await response.json()
                setFormData({
                    automobile: '',
                    salesperson: '',
                    customer: '',
                    price: '',
                })
                getSales()
                navigate('/sales')
            }
        }
    }


    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                    <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.automobile} required id="automobile" name= "automobile" className="form-select">
                            <option value="">Choose a automobile VIN...</option>
                            {automobiles.map(automobile => {
                                if (automobile.sold === false) {
                                return (
                                    <option key={automobile.vin} value={automobile.vin}>
                                        {automobile.vin}
                                    </option>
                                )};
                        })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.salesperson} required id="salesperson" name= "salesperson" className="form-select">
                            <option value="">Choose a salesperson...</option>
                            {salespeople.map(salesperson => {
                                return (
                                    <option key={salesperson.id} value={salesperson.id}>
                                        {salesperson.first_name + " " + salesperson.last_name}
                                    </option>
                                );
                        })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.customer} required id="customer" name= "customer" className="form-select">
                            <option value="">Choose a customer...</option>
                            {customers.map(customer => {
                                return (
                                    <option key={customer.id} value={customer.id}>
                                        {customer.first_name + ' ' + customer.last_name}
                                    </option>
                                );
                        })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.price} placeholder="Price" required type="number" name="price" id="price" className="form-control"/>
                        <label htmlFor="price">0</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        </div>
    )

}

export default SaleForm;