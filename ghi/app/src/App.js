import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ModelList from './ModelList';
import ModelForm from './ModelForm';
import AutomobileForm from './AutomobileForm';
import AutomobilesList from './AutomobilesList';
import Salespeople from './Salespeople';
import SalespersonForm from './SalespersonForm';
import Customers from './Customers';
import CustomerForm from './CustomerForm';
import SaleForm from './SaleForm';
import Sales from './Sales';
import SalespersonHistory from './SalespersonHistory';
import AppointmentForm from './AppointmentForm';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';



function App() {
  const [models, setModels] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [sales, setSales] = useState([]);


  const [technicians, setTechnicians] = useState([]);
  const [appointments, setAppointments] = useState([]);


    async function getModels() {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const {models} = await response.json();
            setModels(models);
        } else {
            console.log('Error fetching models');
        }
    }
    async function getAutomobiles() {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const {autos} = await response.json();
            setAutomobiles(autos);
        } else {
          console.log('Error fetching automobiles');
        }
    }
    async function getManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if(response.ok){
            const {manufacturers} = await response.json();
            setManufacturers(manufacturers);
        } else {
          console.log('Error fetching manufacturers');
        }
    }
    async function getTechnicians() {
      const response = await fetch('http://localhost:8080/api/technicians/')
      if (response.ok) {
          const {technicians} = await response.json()
          setTechnicians(technicians)
      } else {
          console.log('Error fetching technicians');
      }
  }
  async function getAppointments() {
    const response = await fetch('http://localhost:8080/api/appointments/')
    if (response.ok){
      const {appointments} = await response.json();
      setAppointments(appointments);
    } else {
      console.log('Error fetching appointments');
    }
  }

    async function getSalespeople() {
      const response = await fetch('http://localhost:8090/api/salespeople/')
      if (response.ok){
        const salespeople = await response.json();
        setSalespeople(salespeople);
      } else {
        console.log('Error fetching salespeople');
      }
    }
    async function getCustomers() {
      const response = await fetch('http://localhost:8090/api/customers/')
      if (response.ok){
        const customers = await response.json();
        setCustomers(customers);
      } else {
        console.log('Error fetching customers');
      }
    }
    async function getSales() {
      const response = await fetch('http://localhost:8090/api/sales/')
      if (response.ok){
        const sales = await response.json();
        setSales(sales);
      } else {
        console.log('Error fetching sales');
      }
    }


    useEffect(() => {
        getModels();
        getAutomobiles();
        getManufacturers();
        getTechnicians();
        getAppointments();
        getSalespeople();
        getCustomers();
        getSales();
    }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturerList manufacturers={manufacturers} />}/>
            <Route path="new" element={<ManufacturerForm getManufacturers={getManufacturers} />}/>
          </Route>
          <Route path='models'>
            <Route index element={<ModelList models={models} />} />
            <Route path='new' element={<ModelForm getModels={getModels} />} />
          </Route>
          <Route path='automobiles'>
            <Route index element={<AutomobilesList automobiles={automobiles} />} />
            <Route path='new' element={<AutomobileForm models={models} getAutomobiles={getAutomobiles} />} />
          </Route>
          <Route path='salesperson'>
            <Route index element={<Salespeople salespeople={salespeople} />} />
            <Route path='new' element={<SalespersonForm getSalespeople={getSalespeople} />} />
          </Route>
          <Route path='customers'>
            <Route index element={<Customers customers={customers} />} />
            <Route path='new' element={<CustomerForm getCustomers={getCustomers} />} />
          </Route>
          <Route path='sales'>
            <Route index element={<Sales sales={sales} />} />
            <Route path='new' element={<SaleForm getSales={getSales} automobiles={automobiles} salespeople={salespeople} customers={customers} />} />
            <Route path='history' element={<SalespersonHistory sales={sales} salespeople={salespeople} />} />
          </Route>
          <Route path='technicians'>
            <Route index element={<TechnicianList technicians={technicians}/>} />
            <Route path='new' element={<TechnicianForm getTechnicians={getTechnicians} />} />
          </Route>
          <Route path='appointments'>
            <Route index element={<AppointmentList appointments={appointments} getAppointments={getAppointments}/>} />
            <Route path='new' element={<AppointmentForm technicians={technicians} getAppointments={getAppointments} />} />
            <Route path='history' element={<ServiceHistory appointments={appointments}/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
