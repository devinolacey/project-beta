import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

function ModelForm ({getModels}) {
    const navigate = useNavigate()
    const [manufacturers, setManufacturers] = useState([])
    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    })


    async function getManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/')
        if (response.ok) {
            const manufacturers = await response.json()
            setManufacturers(manufacturers.manufacturers)
        } else {
            console.log('Error fetching manufacturers')
        }
    }

    const handleFormChange = (e) => {
        const inputName = e.target.name
        const value = e.target.value
        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const modelUrl = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(modelUrl, fetchConfig)
        if (response.ok) {
            const newModel = await response.json()
            setFormData({
                name: '',
                picture_url: '',
                manufacturer_id: '',
            })
            getModels()
            navigate('/models')
        }
        
    }

    useEffect(() => {
        getManufacturers();
    }, []);
    

    return(
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a vehicle model</h1>
                    <form onSubmit={handleSubmit} id="create-model-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.name} placeholder="Model Name" required type="text" name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.picture_url} placeholder="Picture"  type="url" name="picture_url" id="picture_url" className="form-control"/>
                        <label htmlFor="picture_url">Picture</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.bin} required id="manufacturer_id" name= "manufacturer_id" className="form-select">
                            <option value="">Choose a manufacturer</option>
                            {manufacturers.map(manufacturer => {
                                return (
                                    <option key={manufacturer.id} value={manufacturer.id}>
                                        {manufacturer.name}
                                    </option>
                                );
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        </div>
    )
}

export default ModelForm