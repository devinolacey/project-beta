from django.urls import path
from .views import technician, appointments, canceled, finished

urlpatterns = [
    path("technicians/", technician, name="technican"),
    path("technicians/<int:technician_id>/", technician, name="technician_delete_info"),
    path("appointments/", appointments, name="appointment"),
    path("appointments/<int:appointment_id>/", appointments, name="appoinment_delete_info"),
    path("appointments/<int:appointment_id>/cancel/", canceled, name="cancel"),
    path("appointments/<int:appointment_id>/finish/", finished, name="finish"),
]
