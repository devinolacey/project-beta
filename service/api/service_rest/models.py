from django.db import models

# Create your models here.

class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=20)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField()


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    vip = models.BooleanField(default=False)
    customer = models.CharField(max_length=200)
    date = models.DateField()
    time = models.TimeField()
    technician = models.ForeignKey(
        "Technician",
        related_name='technician',
        on_delete=models.CASCADE,
    )
    reason = models.TextField()
    status = models.CharField(max_length=20, null=True, blank=True)
